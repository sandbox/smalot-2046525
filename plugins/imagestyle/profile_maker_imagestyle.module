<?php

/**
 * @file
 * Profile maker : imagestyle module.
 */

/**
 * Implements pme_info hook alter.
 *
 * @param array $infos
 *   Informations.
 * @param array $context
 *   Context.
 */
function profile_maker_imagestyle_pme_info_alter(&$infos, $context = NULL) {

  $infos['imagestyle'] = array(
    'module'  => 'profile_maker_imagestyle',
    'label'   => 'Image style',
    'weight'  => 15,
    'storage' => array('imagestyle'),
  );
}

/**
 * Implements pme_edit hook alter.
 *
 * @param array $panels
 *   Panels.
 * @param array $context
 *   Context.
 */
function profile_maker_imagestyle_pme_edit_alter(&$panels, $context = NULL) {

  $header = array(
    'label' => t('Image style'),
  );

  $options = array();
  $styles  = image_styles();

  foreach ($styles as $style => $info) {
    // Storage from DB.
    if ($info['storage'] == 1) {
      $options[$style] = array(
        'label' => $info['name'],
      );
    }
  }

  if (isset($context['environment']['settings']['imagestyle_list'])) {
    $default_value = array_fill_keys($context['environment']['settings']['imagestyle_list'], TRUE);
  }
  else {
    $default_value = array();
  }

  foreach ($default_value as $key => $selected) {
    if ($selected && isset($options[$key])) {
      $options[$key]['#attributes'] = array('class' => array('selected'));
    }
  }

  $form['imagestyle_list'] = array(
    '#type'          => 'tableselect',
    '#header'        => $header,
    '#options'       => $options,
    '#multiple'      => TRUE,
    '#empty'         => t('No content available.'),
    '#default_value' => $default_value,
    '#attributes'    => array('class' => array('profile_maker')),
  );

  $panels['imagestyle'] = $form;
}

/**
 * Add files to the profile export.
 *
 * @param array $files
 *   Files.
 * @param array $context
 *   Context.
 */
function profile_maker_imagestyle_pme_export_alter(&$files, $context = NULL) {

  $settings = $context['settings'];
  $content  = array();

  foreach ($settings['imagestyle_list'] as $style => $enabled) {
    if ($enabled) {
      $info      = image_style_load($style);
      $content[] = '$styles[] = drupal_json_decode(\'' . drupal_json_encode($info) . '\');';
    }
  }

  if ($content) {
    array_unshift($content, '$styles = array();');
    $files['profile://data/' . $context['env_name'] . '/imagestyle.php'] = "<?php\n\n" . trim(implode("\n", $content));
  }
}

/**
 * Handle files import.
 *
 * @param array $files
 *   Files.
 * @param array $context
 *   Context.
 */
function profile_maker_imagestyle_pme_import(&$files, $context = NULL) {

  foreach ($files as $position => $file) {

    // Only specific file.
    if (basename($file) == 'imagestyle.php') {

      // Load modules list to enable.
      $styles = array();
      require $file;

      foreach ($styles as $style) {

        // Remove isid to generate a new image styles id.
        unset($style['isid']);
        $style = image_style_save($style);

        foreach ($style['effects'] as $effect) {

          // Remove ieid and set the new isid to respect new dependency.
          $effect['isid'] = $style['isid'];
          unset($effect['ieid']);
          image_effect_save($effect);
        }
      }

      // Remove the file from import list
      // to avoid double import.
      unset($files[$position]);
    }
  }
}

<?php

/**
 * @file
 * Profile maker : imce module.
 */

/**
 * Implements pme_info hook alter.
 *
 * @param array $infos
 *   Informations.
 * @param array $context
 *   Context.
 */
function profile_maker_imce_pme_info_alter(&$infos, $context = NULL) {

  $infos['imce'] = array(
    'module'  => 'profile_maker_imce',
    'label'   => 'IMCE',
    'weight'  => 15,
    'storage' => array('imce'),
  );
}

/**
 * Implements pme_edit hook alter.
 *
 * @param array $panels
 *   Panels.
 * @param array $context
 *   Context.
 */
function profile_maker_imce_pme_edit_alter(&$panels, $context = NULL) {

  $header = array(
    'label' => t('Usage type'),
  );

  $options = array();
  $types   = db_select('file_usage', 'f')
    ->fields('f', array('type'))
    ->condition('module', 'imce')
    ->groupBy('type')
    ->execute()
    ->fetchAllAssoc('type');

  foreach ($types as $type => $info) {
    $options[$type] = array(
      'label' => t('%name (%type)', array('%name' => ucfirst(str_replace('_', ' ', $type)), '%type' => $type)),
    );
  }

  if (isset($context['environment']['settings']['imce_list'])) {
    $default_value = array_fill_keys($context['environment']['settings']['imce_list'], TRUE);
  }
  else {
    $default_value = array();
  }

  foreach ($default_value as $key => $selected) {
    if ($selected && isset($options[$key])) {
      $options[$key]['#attributes'] = array('class' => array('selected'));
    }
  }

  $form['imce_list'] = array(
    '#type'          => 'tableselect',
    '#header'        => $header,
    '#options'       => $options,
    '#multiple'      => TRUE,
    '#empty'         => t('No content available.'),
    '#default_value' => $default_value,
    '#attributes'    => array('class' => array('profile_maker')),
  );

  $panels['imce'] = $form;
}

/**
 * Add files to the profile export.
 *
 * @param array $files
 *   Files.
 * @param array $context
 *   Context.
 */
function profile_maker_imce_pme_export_alter(&$files, $context = NULL) {

  $settings       = $context['settings'];
  $profile_folder = trim($context['settings']['profile_folder'], ' /');
  $infos          = array();

  foreach ($settings['imce_list'] as $type => $enabled) {
    if ($enabled) {
      $fileIds = db_select('file_usage', 'f')
        ->fields('f', array('fid', 'module', 'type', 'id', 'count'))
        ->condition('module', 'imce')
        ->condition('type', $type)
        ->execute()
        ->fetchAllAssoc('fid');

      foreach ($fileIds as $id => $info) {
        $file = file_load($id);

        if ($file) {
          $extension = pathinfo($file->uri, PATHINFO_EXTENSION);
          $filename  = 'file_' . $file->fid . '.' . ($extension ? $extension : 'bin');
          
          if (!isset($infos[$id])) {
            $infos[$id] = array(
              'filename' => $filename,
              'file'     => $file,
              'usages'   => array($info),
            );

            $filename = 'profile://data/' . $context['env_name'] . '/files/' . $filename;
            $filename = profile_maker_generate_resolve_path($filename, $profile_folder, TRUE);
            file_put_contents($filename, file_get_contents($file->uri));
          }
          else {
            $infos[$id]['usages'][] = $info;
          }
        }
      }
    }
  }

  $content = array();

  foreach ($infos as $id => $info) {
    $content[] = '$infos[' . $id . '] = drupal_json_decode(\'' . drupal_json_encode($info) . '\');';
  }

  if ($content) {
    array_unshift($content, '$infos = array();');
    $files['profile://data/' . $context['env_name'] . '/imce.php'] = "<?php\n\n" . trim(implode("\n", $content));
  }
}

/**
 * Handle files import.
 *
 * @param array $files
 *   Files.
 * @param array $context
 *   Context.
 */
function profile_maker_imce_pme_import(&$files, $context = NULL) {

  foreach ($files as $position => $file) {

    // Only specific file.
    if (basename($file) == 'imce.php') {

      $infos = array();
      require $file;

      foreach ($infos as $info) {
        // Destination directory.
        $dir = drupal_dirname($info['file']['uri']);

        if (file_prepare_directory($dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
          // Source file.
          $filename = $context['folder'] . '/data/' . $context['env_name'] . '/files/' . $info['filename'];

          if (file_exists($filename)) {
            $file = file_save_data(file_get_contents($filename), $info['file']['uri'], FILE_EXISTS_REPLACE);

            if ($file) {
              foreach ($info['usages'] as $usage) {
                // Update usage array according to new file inserted.
                $usage['fid'] = $file->fid;
                $usage['id']  = $file->fid;

                db_insert('file_usage')
                  ->fields(array('fid', 'module', 'type', 'id', 'count'))
                  ->values($usage)
                  ->execute();
              }
            }
          }
        }
      }

      // Remove the file from import list
      // to avoid double import.
      unset($files[$position]);
    }
  }
}

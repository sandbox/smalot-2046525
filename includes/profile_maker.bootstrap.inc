<?php
/**
 * @file
 * Profile maker bootstrap used on profile setup.
 */

/**
 * Callback function on init profile setup.
 *
 * @param string $env_name
 *   Environment name.
 * @param string $folder
 *   Folder holding all setup data.
 */
function profile_maker_bootstrap_import($env_name, $folder) {

  module_enable(array('profile_maker_module'), TRUE);

  // Select all files in the folder.
  $regex = '^.*\.php$';
  $files = file_scan_directory($folder . '/data/' . $env_name, '/' . $regex . '/');
  foreach ($files as $key => $file) {
    if ($file->filename != ($env_name . '/init.php')) {
      $files[$key] = $file->uri;
    }
    else {
      unset($files[$key]);
    }
  }

  $context = array(
    'env_name' => $env_name,
    'folder'   => $folder,
  );

  // Force import of variables.
  if (function_exists('profile_maker_variables_pme_import')) {
    profile_maker_variables_pme_import($files, $context);
  }

  // Force import of module in first.
  if (function_exists('profile_maker_module_pme_import')) {
    profile_maker_module_pme_import($files, $context);
  }

  // Import of contents
  // - nodes
  // - files/images
  // - url_alias
  $infos = array();
  drupal_alter('pme_info', $infos, $context);

  $plugins = array();
  foreach ($infos as $info) {
    $weight                                   = (isset($info['weight']) ? $info['weight'] : 99);
    $plugins[$weight . '-' . $info['module']] = $info;
  }
  ksort($plugins, SORT_NUMERIC);

  // Call order forced functions
  // (lower weight is called first)
  // hook_pme_import.
  foreach ($plugins as $info) {
    $function = $info['module'] . '_pme_import';
    if (function_exists($function)) {
      $function($files, $context);
    }
  }

  // Call standard functions
  // hook_pme_import_alter.
  drupal_alter('pme_import', $files, $context);
}

<?php
/**
 * @file
 * Manage profile generation.
 */

/**
 * Profile generation form.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 */
function profile_maker_generate($form, &$form_state) {

  $environments = profile_maker_list();
  $options      = array();

  foreach ($environments as $env_name => $title) {
    $options[$env_name] = array(
      'label' => check_plain($title),
    );
  }

  $header = array(
    'label' => t('Label'),
  );

  $form['list'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Environments'),
    '#collapsible' => TRUE,
  );

  $default = variable_get('profile_maker_default_env', key($options));

  $form['list']['env_name'] = array(
    '#type'          => 'tableselect',
    '#header'        => $header,
    '#options'       => $options,
    '#multiple'      => FALSE,
    '#empty'         => t('No content available.'),
    '#default_value' => $default,
    '#attributes'    => array('class' => array('profile_maker')),
  );

  if (count($options)) {
    $form['submit'] = array(
      '#type'  => 'submit',
      '#value' => t('Select'),
    );
  }

  return $form;
}

/**
 * Profile generation submit callback.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 */
function profile_maker_generate_submit($form, &$form_state) {

  $values      = $form_state['values'];
  $env_name    = $values['env_name'];
  $environment = profile_maker_load($env_name);

  if (!$environment) {
    drupal_set_message(t('Missing environment'), 'error');

    return;
  }

  variable_set('profile_maker_default_env', $env_name);

  $files          = array();
  $context        = array(
    'env_name'     => $env_name,
    'title'        => $environment['title'],
    'settings'     => $environment['settings'],
  );
  $profile_folder = trim($environment['settings']['profile_folder'], ' /');

  // Ask all plugins to generate content.
  drupal_alter('pme_export', $files, $context);
  ksort($files);

  // Write all files.
  foreach ($files as $filename => $content) {
    $filename = profile_maker_generate_resolve_path($filename, $profile_folder, TRUE);

    if (!$filename) {
      return;
    }

    // Write content to file.
    if (!file_unmanaged_save_data($content, $filename, FILE_EXISTS_REPLACE)) {
      drupal_set_message(t('Unable to create destination file: :file', array(':file' => $filename)), 'error');

      return;
    }
    else {
      drupal_set_message(
        t('File create: :file (:size bytes)',
          array(
            ':file' => $filename,
            ':size' => filesize($filename),
          )
        )
      );
    }
  }
}

/**
 * Resolve path.
 *
 * @param string $path
 *   Path to resolve.
 * @param string $profile_folder
 *   Profile directory.
 * @param bool   $create_folder
 *   Indicate if we create folder.
 *
 * @return string
 *   Path resolved.
 */
function profile_maker_generate_resolve_path($path, $profile_folder, $create_folder = FALSE) {
  $path = str_replace('profile://', DRUPAL_ROOT . '/profiles/' . $profile_folder . '/', $path);

  if ($create_folder) {
    $dir = dirname($path);

    // Create destination folder is not exists.
    if (!is_dir($dir)) {
      if (!drupal_mkdir($dir, NULL, TRUE)) {
        drupal_set_message(t('Unable to create destination folder: :folder', array(':folder' => $dir)), 'error');

        return NULL;
      }
    }
  }

  return $path;
}

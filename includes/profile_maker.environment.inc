<?php
/**
 * @file
 * Manage pages for environments.
 */

/**
 * List existing environment and form to add more.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 *
 * @return array
 *   Form.
 */
function profile_maker_environment_add($form, &$form_state) {

  $environments = profile_maker_list();

  $rows = array();
  foreach ($environments as $env_name => $title) {
    $rows[] = array(
      'title'  => check_plain($title),
      'edit'   => l(t('Edit'), 'admin/config/development/profile_maker/environments/edit/' . $env_name),
      'delete' => l(t('Delete'), 'admin/config/development/profile_maker/environments/delete/' . $env_name),
    );
  }

  $header = array(t('Name'), array('data' => t('Operation'), 'colspan' => '3'));

  $form['list'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Environments'),
    '#collapsible' => TRUE,
  );

  $form['list']['table'] = array(
    '#theme'  => 'table',
    '#header' => $header,
    '#rows'   => $rows,
    '#empty'  => t('No content available.'),
  );

  $form['item'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Add environment'),
  );

  $form['item']['title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Title'),
    '#required'      => TRUE,
    '#description'   => t("user-friendly name."),
    '#size'          => 40,
    '#maxlength'     => 127,
    '#default_value' => '',
  );

  $form['item']['env_name'] = array(
    '#type'          => 'machine_name',
    '#title'         => t('Machine Name'),
    '#required'      => TRUE,
    '#description'   => t('A unique name.'),
    '#size'          => 30,
    '#maxlength'     => 64,
    '#default_value' => '',
    '#machine_name'  => array(
      // Function that return 1 if the machine name is duplicated.
      'exists'          => 'profile_maker_exists',
      // The name of the source field that we will Take the User Friendly name,
      // from and convert it to Machine Friendly name.
      'source'          => array('title'),
      'replace_pattern' => '[^a-z0-9-\_]+',
      'replace'         => '-',
    ),
  );

  $form['item']['op'] = array(
    '#type'  => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Submit hook for new environment.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 */
function profile_maker_environment_add_submit($form, &$form_state) {

  try {
    $env_name = $form_state['values']['env_name'];
    $title    = $form_state['values']['title'];

    $environment = array(
      'env_name' => $env_name,
      'title'    => $title,
    );

    profile_maker_save($environment);

  }
  catch (Exception $e) {
    if ($e->getCode() == 23000) {
      drupal_set_message(t('The environment name already exists.'), 'error');
    }
    else {
      drupal_set_message($e->getCode(), 'error');
    }
  }
}

/**
 * Form edit.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 * @param string $env_name
 *   Environment name.
 */
function profile_maker_environment_edit($form, &$form_state, $env_name) {

  $environment = profile_maker_load($env_name);

  if (!$environment) {
    drupal_goto('admin/config/development/profile_maker/environments');
  }

  $profile_folder = (isset($environment['settings']['profile_folder']) ? $environment['settings']['profile_folder'] : 'my_profile');

  $form['profile']['profile_folder'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Profile folder'),
    '#required'      => TRUE,
    '#description'   => t("Profile's folder under which files will be stored (under 'profiles/')."),
    '#size'          => 40,
    '#maxlength'     => 127,
    '#default_value' => $profile_folder,
  );

  $form['tabs'] = array(
    '#type'   => 'vertical_tabs',
    '#weight' => 99,
  );

  $context = array(
    'env_name'    => $env_name,
    'environment' => $environment,
    'settings'    => $environment['settings'],
  );

  $infos = array();
  drupal_alter('pme_info', $infos, $context);

  $panels = array();
  drupal_alter('pme_edit', $panels, $context);

  foreach ($panels as $plugin => $panel) {
    if ($infos[$plugin]) {
      $form[$plugin] = array(
        '#type'  => 'fieldset',
        '#title' => t($infos[$plugin]['label']),
      );
      $form[$plugin] += $panel;
    }
  }

  foreach ($form as $key => $element) {
    if (isset($element['#type']) && $element['#type'] == 'fieldset') {
      $form[$key]['#group']  = 'tabs';
      $form[$key]['#weight'] = isset($infos[$key]['weight']) ? $infos[$key]['weight'] : 99;
    }
  }

  $form['item']['env_name'] = array(
    '#type'  => 'hidden',
    '#value' => $env_name,
  );

  $form['item']['op'] = array(
    '#type'  => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

/**
 * Form edit submit callback.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 */
function profile_maker_environment_edit_submit($form, &$form_state) {

  $values      = $form_state['values'];
  $env_name    = $values['env_name'];
  $environment = profile_maker_load($env_name);

  if (!$environment) {
    drupal_goto('admin/config/development/profile_maker/environments');
  }

  // For standard plugin.
  $context = array(
    'env_name'    => $env_name,
    'environment' => $environment,
    'settings'    => $environment['settings'],
  );
  $infos   = array();
  drupal_alter('pme_info', $infos, $context);

  foreach ($infos as $info) {
    if (isset($info['storage']) && !empty($info['storage'])) {
      _profile_maker_filter_keys($environment['settings'], $values, $info['storage']);
    }
  }

  // Stored also internal profile values.
  _profile_maker_filter_keys($environment['settings'], $values, array('profile'));

  // For more complex values recording, a helper is called.
  $context = array(
    'env_name' => $env_name,
    'values'   => $values,
  );
  drupal_alter('pme_save', $environment['settings'], $context);

  // Update database.
  profile_maker_save($environment);

  drupal_goto('admin/config/development/profile_maker/environments');
}

/**
 * Delete form.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 * @param string $env_name
 *   Environment name.
 *
 * @return array
 *   Form.
 */
function profile_maker_environment_delete($form, &$form_state, $env_name) {

  $environment = profile_maker_load($env_name);

  if (!$environment) {
    drupal_goto('admin/config/development/profile_maker/environments');
  }

  $form['env_name'] = array(
    '#type'  => 'hidden',
    '#value' => $env_name,
  );

  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $environment['title'])),
    'admin/config/development/profile_maker/environments',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit callback.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 */
function profile_maker_environment_delete_submit($form, &$form_state) {

  $env_name = $form_state['values']['env_name'];

  profile_maker_del($env_name);

  drupal_goto('admin/config/development/profile_maker/environments');
}

/**
 * Filters settings on update environment callback.
 *
 * @param array $settings
 *   Settings.
 * @param array $values
 *   Values.
 * @param array $supported_keys
 *   Supported keys.
 */
function _profile_maker_filter_keys(&$settings, $values, $supported_keys) {

  $supported_keys = array_map('preg_quote', $supported_keys);
  $regex          = '/^' . implode('\_|', $supported_keys) . '\_[a-zA-Z0-9]+$/';

  foreach ($values as $key => $value) {
    if (preg_match($regex, $key)) {
      $settings[$key] = $value;
    }
  }
}

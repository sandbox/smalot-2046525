
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Cache Warning
 * Design Decisions


INTRODUCTION
------------

Current Maintainer: Sebastien MALOT <sebastien@malot.fr>

Profile Maker is an assistant to help you to generate an installation profile.
Activate each sub-module and select each element you want to export to the
profile.

The aim of this module is to duplicate a working site and store it into files.
Once it's done, your can reinstall it under a new server easily using for
example the drush command 'site-install'.

This is very useful in continuous integration environment.

Keep in mind this module is a toolbox.
Use it, test it, improve it.


INSTALLATION
------------

1. This module is under config/development/profile_maker administration menu.

2. Go to the configure user interface and create a new 'environment'.
Create as many environment as you want.

For example :
- one for your development with the 'devel' module enabled
- another for your production without the 'devel' module enabled

3. Once you have created and configured an environment, your profile can be
generated.

4. Your profile is built under the 'profiles' folder, with the following
folders :
- data : which contains all standard files
- modules/custom : which contains ctools generated module

5. To use 'Profile Maker' in your profile setup, you have to add it into your 
'module/contrib' folder.

6. You have to add the bootstrap file into your profile installation code :
'profile_maker.bootstrap.inc'.

According to the environment name passed to the
'profile_maker_bootstrap_import' function and the folder, this function
will call all import functions.

7. That's all folks.


CACHE WARNING
-------------

No information.


DESIGN DECISIONS
----------------

'Profile Maker' has been designed to be extensible. You can create your own
sub-module to export/import both config and data of your favorites modules.

Note : For maintenability, it is adviced to create a sub-module for
each module you want to export.

Steps :
-------

1. Create a new folder (eg : my_export_import)

2. Create the according module info file (eg : my_export_import.info)

----------------------------------------------
name = My own export/import module
description = 
package = Profile Maker
core = 7.x
dependencies[] = profile_maker
----------------------------------------------

3. Create the according module code (eg : my_export_import.module)

----------------------------------------------
<?php

/**
 * @file
 * my_export_import module.
 */

/**
 * Implements pme_info hook alter.
 *
 * @param array $infos
 *   Informations.
 * @param array $context
 *   Context.
 */
function my_export_import_pme_info_alter(&$infos, $context = NULL) {

  $infos['menu'] = array(
    'module'  => 'my_export_import',
    'label'   => 'Menu',
    'weight'  => 90,
    'storage' => array('my_export_import'),
  );
}


/**
 * Implements pme_edit hook alter.
 *
 * @param array $panels
 *   Panels.
 * @param array $context
 *   Context.
 */
function my_export_import_pme_edit_alter(&$panels, $context = NULL) {
  $form = array();

  // TODO : Create a form and under a named key called 'my_export_import'
  // according to the 'storage' field returned by the pme_info_alter hook
  // to the panels variable.
  $panels['my_export_import'] = $form;
}

/**
 * Add files to the profile export.
 *
 * @param array $files
 *   Files.
 * @param array $context
 *   Context.
 */
function my_export_import_pme_export_alter(&$files, $context = NULL) {

  // This variable hold all environment properties.
  $settings = $context['settings'];

  // The content of the file.
  $content = '';

  // TODO : build the content to store into the file.
  $pathfile         = 'profile://data/' .
                      $context['env_name'] .
                      '/my_export_import.php';
  $files[$pathfile] = $content;
}

/**
 * Handle files import.
 *
 * @param array $files
 *   Files.
 * @param array $context
 *   Context.
 */
function my_export_import_pme_import(&$files, $context = NULL) {

  foreach ($files as $position => $file) {

    // Only specific file.
    if (basename($file) == 'my_export_import.php') {
      // TODO : handle import.
      // Remove the file from import list
      // to avoid double import.
      unset($files[$position]);
    }
  }
}
?>
----------------------------------------------

4. Enable your module and setup it in your environment.
